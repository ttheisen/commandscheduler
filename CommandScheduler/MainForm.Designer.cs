﻿namespace CommandScheduler {
    partial class MainForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CommandTextbox = new System.Windows.Forms.TextBox();
            this.CommandBrowseButton = new System.Windows.Forms.Button();
            this.ScheduleTable = new System.Windows.Forms.TableLayoutPanel();
            this.DayLabel = new System.Windows.Forms.Label();
            this.StartLabel = new System.Windows.Forms.Label();
            this.EndLabel = new System.Windows.Forms.Label();
            this.CommandPathLabel = new System.Windows.Forms.Label();
            this.IntervalLabel = new System.Windows.Forms.Label();
            this.IntervalInput = new System.Windows.Forms.NumericUpDown();
            this.SaveButton = new System.Windows.Forms.Button();
            this.StatusBar = new System.Windows.Forms.StatusStrip();
            this.StatusBarLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.ScheduleTable.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalInput)).BeginInit();
            this.StatusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // CommandTextbox
            // 
            this.CommandTextbox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandTextbox.Location = new System.Drawing.Point(12, 32);
            this.CommandTextbox.Name = "CommandTextbox";
            this.CommandTextbox.Size = new System.Drawing.Size(554, 26);
            this.CommandTextbox.TabIndex = 0;
            // 
            // CommandBrowseButton
            // 
            this.CommandBrowseButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CommandBrowseButton.Location = new System.Drawing.Point(572, 32);
            this.CommandBrowseButton.Name = "CommandBrowseButton";
            this.CommandBrowseButton.Size = new System.Drawing.Size(30, 35);
            this.CommandBrowseButton.TabIndex = 1;
            this.CommandBrowseButton.Text = "...";
            this.CommandBrowseButton.UseVisualStyleBackColor = true;
            // 
            // ScheduleTable
            // 
            this.ScheduleTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ScheduleTable.ColumnCount = 3;
            this.ScheduleTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ScheduleTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ScheduleTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.ScheduleTable.Controls.Add(this.DayLabel, 0, 0);
            this.ScheduleTable.Controls.Add(this.StartLabel, 1, 0);
            this.ScheduleTable.Controls.Add(this.EndLabel, 2, 0);
            this.ScheduleTable.Location = new System.Drawing.Point(12, 163);
            this.ScheduleTable.Name = "ScheduleTable";
            this.ScheduleTable.RowCount = 1;
            this.ScheduleTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.ScheduleTable.Size = new System.Drawing.Size(590, 347);
            this.ScheduleTable.TabIndex = 2;
            // 
            // DayLabel
            // 
            this.DayLabel.AutoSize = true;
            this.DayLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DayLabel.Location = new System.Drawing.Point(3, 0);
            this.DayLabel.Name = "DayLabel";
            this.DayLabel.Size = new System.Drawing.Size(40, 20);
            this.DayLabel.TabIndex = 0;
            this.DayLabel.Text = "Day";
            // 
            // StartLabel
            // 
            this.StartLabel.AutoSize = true;
            this.StartLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StartLabel.Location = new System.Drawing.Point(49, 0);
            this.StartLabel.Name = "StartLabel";
            this.StartLabel.Size = new System.Drawing.Size(49, 20);
            this.StartLabel.TabIndex = 1;
            this.StartLabel.Text = "Start";
            // 
            // EndLabel
            // 
            this.EndLabel.AutoSize = true;
            this.EndLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EndLabel.Location = new System.Drawing.Point(104, 0);
            this.EndLabel.Name = "EndLabel";
            this.EndLabel.Size = new System.Drawing.Size(41, 20);
            this.EndLabel.TabIndex = 2;
            this.EndLabel.Text = "End";
            // 
            // CommandPathLabel
            // 
            this.CommandPathLabel.AutoSize = true;
            this.CommandPathLabel.Location = new System.Drawing.Point(8, 9);
            this.CommandPathLabel.Name = "CommandPathLabel";
            this.CommandPathLabel.Size = new System.Drawing.Size(119, 20);
            this.CommandPathLabel.TabIndex = 3;
            this.CommandPathLabel.Text = "Command Path";
            // 
            // IntervalLabel
            // 
            this.IntervalLabel.AutoSize = true;
            this.IntervalLabel.Location = new System.Drawing.Point(12, 74);
            this.IntervalLabel.Name = "IntervalLabel";
            this.IntervalLabel.Size = new System.Drawing.Size(205, 20);
            this.IntervalLabel.TabIndex = 4;
            this.IntervalLabel.Text = "Execution Interval (minutes)";
            // 
            // IntervalInput
            // 
            this.IntervalInput.Location = new System.Drawing.Point(13, 98);
            this.IntervalInput.Name = "IntervalInput";
            this.IntervalInput.Size = new System.Drawing.Size(120, 26);
            this.IntervalInput.TabIndex = 5;
            this.IntervalInput.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // SaveButton
            // 
            this.SaveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveButton.Location = new System.Drawing.Point(12, 520);
            this.SaveButton.Name = "SaveButton";
            this.SaveButton.Size = new System.Drawing.Size(151, 44);
            this.SaveButton.TabIndex = 6;
            this.SaveButton.Text = "Apply Changes";
            this.SaveButton.UseVisualStyleBackColor = true;
            this.SaveButton.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // StatusBar
            // 
            this.StatusBar.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.StatusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusBarLabel});
            this.StatusBar.Location = new System.Drawing.Point(0, 575);
            this.StatusBar.Name = "StatusBar";
            this.StatusBar.Size = new System.Drawing.Size(614, 30);
            this.StatusBar.TabIndex = 7;
            this.StatusBar.Text = "StatusBar";
            // 
            // StatusBarLabel
            // 
            this.StatusBarLabel.Name = "StatusBarLabel";
            this.StatusBarLabel.Size = new System.Drawing.Size(59, 25);
            this.StatusBarLabel.Text = "status";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 605);
            this.Controls.Add(this.StatusBar);
            this.Controls.Add(this.SaveButton);
            this.Controls.Add(this.IntervalInput);
            this.Controls.Add(this.IntervalLabel);
            this.Controls.Add(this.CommandPathLabel);
            this.Controls.Add(this.ScheduleTable);
            this.Controls.Add(this.CommandBrowseButton);
            this.Controls.Add(this.CommandTextbox);
            this.Name = "MainForm";
            this.Text = "Command Scheduler";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ScheduleTable.ResumeLayout(false);
            this.ScheduleTable.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IntervalInput)).EndInit();
            this.StatusBar.ResumeLayout(false);
            this.StatusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox CommandTextbox;
        private System.Windows.Forms.Button CommandBrowseButton;
        private System.Windows.Forms.TableLayoutPanel ScheduleTable;
        private System.Windows.Forms.Label CommandPathLabel;
        private System.Windows.Forms.Label IntervalLabel;
        private System.Windows.Forms.Label DayLabel;
        private System.Windows.Forms.Label StartLabel;
        private System.Windows.Forms.Label EndLabel;
        private System.Windows.Forms.NumericUpDown IntervalInput;
        private System.Windows.Forms.Button SaveButton;
        private System.Windows.Forms.StatusStrip StatusBar;
        private System.Windows.Forms.ToolStripStatusLabel StatusBarLabel;
    }
}


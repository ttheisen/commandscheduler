﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json;
using System.Diagnostics;
using System.IO;

namespace CommandScheduler {
    public partial class MainForm : Form {
        private string SettingsPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Application.ProductName, "settings.json");
        private System.Threading.Timer TaskTimer = null;

        private Dictionary<DayOfWeek, TextBox> StartBoxes = new Dictionary<DayOfWeek, TextBox>();
        private Dictionary<DayOfWeek, TextBox> EndBoxes = new Dictionary<DayOfWeek, TextBox>();
        private DayOfWeek[] DaysOfWeek = {
                DayOfWeek.Sunday,
                DayOfWeek.Monday,
                DayOfWeek.Tuesday,
                DayOfWeek.Wednesday,
                DayOfWeek.Thursday,
                DayOfWeek.Friday,
                DayOfWeek.Saturday,
            };
        private CommandState CurrentState = new CommandState();
        
        public MainForm() {
            InitializeComponent();

            var dateFormat = Thread.CurrentThread.CurrentCulture.DateTimeFormat;

            foreach (var day in this.DaysOfWeek) {
                int index = this.ScheduleTable.RowStyles.Add(new RowStyle());

                var dayLabel = new Label { Text = dateFormat.GetDayName(day) };
                this.ScheduleTable.Controls.Add(dayLabel, 0, index);

                var startTextbox = new TextBox{ Dock = DockStyle.Fill };
                this.ScheduleTable.Controls.Add(startTextbox, 1, index);
                this.StartBoxes[day] = startTextbox;

                var endTextbox = new TextBox { Dock = DockStyle.Fill };
                this.ScheduleTable.Controls.Add(endTextbox, 2, index);
                this.EndBoxes[day] = endTextbox;
            }
        }

        private void MainForm_Load(object sender, EventArgs e) {
            Debug.WriteLine(this.SettingsPath, "settings path");
            this.LoadState();
            this.StartExecution(null);
        }

        private void SaveButton_Click(object sender, EventArgs e) {
            this.ApplyChanges();
            this.SaveState();
            this.StartExecution();
        }

        public void ApplyChanges() {
            this.CurrentState = new CommandState {
                CommandPath = this.CommandTextbox.Text,
                ExecutionInterval = Convert.ToInt32(this.IntervalInput.Value),
            };

            foreach (var day in this.DaysOfWeek) {
                TimeSpan start;
                TimeSpan end;
                bool startSuccess = TimeSpan.TryParseExact(this.StartBoxes[day].Text, "h\\:mm", CultureInfo.InvariantCulture.DateTimeFormat, out start);
                bool endSuccess = TimeSpan.TryParseExact(this.EndBoxes[day].Text, "h\\:mm", CultureInfo.InvariantCulture.DateTimeFormat, out end);
                this.CurrentState.ExecutionTimes[day] = new ExecutionTimes {
                    Start = startSuccess ? (TimeSpan?)start : null,
                    End = endSuccess ? (TimeSpan?)end : null,
                };
            }
        }

        public void LoadState() {
            try {
                string serialized = File.ReadAllText(this.SettingsPath);
                this.CurrentState = JsonConvert.DeserializeObject<CommandState>(serialized);

                this.CommandTextbox.Text = this.CurrentState.CommandPath;
                this.IntervalInput.Value = this.CurrentState.ExecutionInterval;

                foreach (var day in this.DaysOfWeek) {
                    this.StartBoxes[day].Text = string.Format("{0:hh\\:mm}", this.CurrentState.ExecutionTimes[day].Start);
                    this.EndBoxes[day].Text = string.Format("{0:hh\\:mm}", this.CurrentState.ExecutionTimes[day].End);
                }

            } catch (Exception ex) {
                MessageBox.Show(ex.Message);
            }
        }

        public void SaveState() {
            string serialized = JsonConvert.SerializeObject(this.CurrentState);
            Debug.WriteLine(serialized, "Saved state");
            Directory.CreateDirectory(Path.GetDirectoryName(this.SettingsPath));
            File.WriteAllText(this.SettingsPath, serialized);
        }

        public void StartExecution(object nothing = null) {
            var times = this.CurrentState.ExecutionTimes[DateTime.Today.DayOfWeek];

            var time = DateTime.Now.TimeOfDay;
            TimeSpan nextCheck = TimeSpan.FromHours(24) - time; // next midnight

            if (times.Start.HasValue && times.End.HasValue) {
                if (time < times.Start.Value) {
                    nextCheck = times.Start.Value - time;
                } else if (time < times.End.Value) {
                    RunProcess();
                    nextCheck = TimeSpan.FromMinutes(this.CurrentState.ExecutionInterval);
                }
            }

            if (this.TaskTimer != null) {
                this.TaskTimer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
            }
            this.TaskTimer = new System.Threading.Timer((TimerCallback)this.StartExecution, null, nextCheck, TimeSpan.FromMilliseconds(-1));

            string message = string.Format("As of {0}, next check is in {1}.", DateTime.Now, nextCheck);
            this.StatusBarLabel.Text = message;
        }

        private void RunProcess() {
            try {
                Process.Start(this.CurrentState.CommandPath);
            } catch (Exception) {

            }
        }
    }
}

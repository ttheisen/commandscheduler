﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandScheduler {
    class CommandState {
        public int ExecutionInterval;
        public string CommandPath;
        public Dictionary<DayOfWeek, ExecutionTimes> ExecutionTimes = new Dictionary<DayOfWeek, ExecutionTimes>();
    }
}
